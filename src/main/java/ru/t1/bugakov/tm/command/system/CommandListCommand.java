package ru.t1.bugakov.tm.command.system;

import ru.t1.bugakov.tm.api.service.ICommandService;
import ru.t1.bugakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final ICommandService commandService = getCommandService();
        final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (AbstractCommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list.";
    }

}
