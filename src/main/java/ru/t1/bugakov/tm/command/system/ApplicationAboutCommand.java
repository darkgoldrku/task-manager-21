package ru.t1.bugakov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Danil Bugakov");
        System.out.println("e-mail: dbugakov@t1-consulting.ru");
        System.out.println("e-mail: darkgoldrku@gmail.com");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show about program.";
    }

}
