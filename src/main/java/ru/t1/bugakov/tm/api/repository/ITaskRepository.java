package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(final String userId, String projectId);

}
